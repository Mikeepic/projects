# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
("#datetimepicker1").datetimepicker
  lang: "de"
  i18n:
    de:
      months: [
        "Januar"
        "Februar"
        "März"
        "April"
        "Mai"
        "Juni"
        "Juli"
        "August"
        "September"
        "Oktober"
        "November"
        "Dezember"
      ]
      dayOfWeek: [

        "Mo"
        "Di"
        "Mi"
        "Do"
        "Fr"

      ]

  timepicker: false
  format: "d.m.Y"
