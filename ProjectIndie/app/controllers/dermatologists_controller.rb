class DermatologistsController < ApplicationController
  before_action :set_dermatologist, only: [:show, :edit, :update, :destroy]

  # GET /dermatologists
  # GET /dermatologists.json
  def index
    @dermatologists = Dermatologist.all
  end

  # GET /dermatologists/1
  # GET /dermatologists/1.json
  def show
  end

  # GET /dermatologists/new
  def new
    @dermatologist = Dermatologist.new
  end

  # GET /dermatologists/1/edit
  def edit
  end

  # POST /dermatologists
  # POST /dermatologists.json
  def create
    @dermatologist = Dermatologist.new(dermatologist_params)

    respond_to do |format|
      if @dermatologist.save
        format.html { redirect_to @dermatologist, notice: 'Dermatologist was successfully created.' }
        format.json { render :show, status: :created, location: @dermatologist }
      else
        format.html { render :new }
        format.json { render json: @dermatologist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dermatologists/1
  # PATCH/PUT /dermatologists/1.json
  def update
    respond_to do |format|
      if @dermatologist.update(dermatologist_params)
        format.html { redirect_to @dermatologist, notice: 'Dermatologist was successfully updated.' }
        format.json { render :show, status: :ok, location: @dermatologist }
      else
        format.html { render :edit }
        format.json { render json: @dermatologist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dermatologists/1
  # DELETE /dermatologists/1.json
  def destroy
    @dermatologist.destroy
    respond_to do |format|
      format.html { redirect_to dermatologists_url, notice: 'Dermatologist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dermatologist
      @dermatologist = Dermatologist.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dermatologist_params
      params.require(:dermatologist).permit(:First_name, :Last_name)
    end
end
