class Appointment < ActiveRecord::Base
  belongs_to :dermatologist
  belongs_to :patient
  belongs_to :time_slot
  belongs_to :diagnostic
  belongs_to :status

end
