class Patient < ActiveRecord::Base

  belongs_to :insurance
  has_many :appointments
  has_many :dermatologists, through: :appointments

end
