json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :date, :time_slot_id, :diagnostic_id, :patient_id, :dermatologist_id, :reason
  json.url appointment_url(appointment, format: :json)
end
