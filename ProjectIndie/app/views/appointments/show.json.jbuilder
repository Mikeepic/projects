json.extract! @appointment, :id, :date, :time_slot_id, :diagnostic_id, :patient_id, :dermatologist_id, :reason, :created_at, :updated_at
