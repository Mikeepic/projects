json.array!(@dermatologists) do |dermatologist|
  json.extract! dermatologist, :id, :First_name, :Last_name
  json.url dermatologist_url(dermatologist, format: :json)
end
