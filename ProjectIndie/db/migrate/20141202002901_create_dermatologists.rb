class CreateDermatologists < ActiveRecord::Migration
  def change
    create_table :dermatologists do |t|
      t.string :First_name
      t.string :Last_name

      t.timestamps
    end
  end
end
