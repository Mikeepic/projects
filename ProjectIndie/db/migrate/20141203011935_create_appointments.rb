class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.date :date
      t.integer :time_slot_id
      t.integer :diagnostic_id
      t.integer :patient_id
      t.integer :dermatologist_id
      t.string :reason

      t.timestamps
    end
  end
end
