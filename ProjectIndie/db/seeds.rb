# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'csv'

TimeSlot.delete_all
CSV.foreach("#{Rails.root}/db/TimeSlots.csv") do |row|
  TimeSlot.create!(:Time => row[0])
end

Diagnostic.delete_all
CSV.foreach("#{Rails.root}/db/DiagnosticCodes.csv") do |row|
  Diagnostic.create!(:code => row[0], :detail => row[1], :amount => row[2])
end
