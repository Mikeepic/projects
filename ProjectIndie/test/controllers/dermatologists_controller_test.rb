require 'test_helper'

class DermatologistsControllerTest < ActionController::TestCase
  setup do
    @dermatologist = dermatologists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dermatologists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dermatologist" do
    assert_difference('Dermatologist.count') do
      post :create, dermatologist: { Last_name: @dermatologist.Last_name, [First_name: @dermatologist.[First_name }
    end

    assert_redirected_to dermatologist_path(assigns(:dermatologist))
  end

  test "should show dermatologist" do
    get :show, id: @dermatologist
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dermatologist
    assert_response :success
  end

  test "should update dermatologist" do
    patch :update, id: @dermatologist, dermatologist: { Last_name: @dermatologist.Last_name, [First_name: @dermatologist.[First_name }
    assert_redirected_to dermatologist_path(assigns(:dermatologist))
  end

  test "should destroy dermatologist" do
    assert_difference('Dermatologist.count', -1) do
      delete :destroy, id: @dermatologist
    end

    assert_redirected_to dermatologists_path
  end
end
